Source: gst-plugins-base1.0
Section: libs
Priority: optional
Maintainer: Raul Tambre <raul@tambre.ee>
Build-Depends: debhelper (>= 13), meson,
    dpkg-dev,
    gir1.2-freedesktop,
    gir1.2-glib-2.0,
    gir1.2-gstreamer-1.0,
    gobject-introspection,
    iso-codes,
    libasound2-dev,
    libcdparanoia-dev,
    libdrm-dev,
    libegl1-mesa-dev,
    libgbm-dev,
    libgirepository1.0-dev,
    libgl1-mesa-dev,
    libgles2-mesa-dev,
    libglib2.0-dev,
    libgraphene-1.0-dev,
    libgstreamer1.0-dev,
    libgtk-3-dev,
    libgudev-1.0-dev,
    libjpeg-dev,
    libopus-dev,
    liborc-0.4-dev,
    libpango1.0-dev,
    libpng-dev,
    libtheora-dev,
    libtool,
    libvisual-0.4-dev,
    libvorbis-dev,
    libwayland-dev,
    libx11-xcb-dev,
    libxt-dev,
    libxv-dev,
    pkg-config,
    wayland-protocols,
    zlib1g-dev,
Rules-Requires-Root: no
Standards-Version: 4.5.0.3
Vcs-Git: https://gitlab.freedesktop.org/gstreamer/gst-plugins-base.git
Vcs-Browser: https://gitlab.freedesktop.org/gstreamer/gst-plugins-base
Homepage: https://gstreamer.freedesktop.org

Package: gstreamer1.0-plugins-base-apps
Section: utils
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, gstreamer1.0-tools
Description: GStreamer helper programs from the "base" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains helper programs from the "base" set, an essential
 exemplary set of elements.

Package: libgstreamer-plugins-base1.0-0
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}, iso-codes
Recommends: gstreamer1.0-plugins-base
Description: GStreamer libraries from the "base" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains libraries from the "base" set, an essential
 exemplary set of elements.

Package: libgstreamer-plugins-base1.0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}
    gir1.2-gst-plugins-base-1.0 (= ${binary:Version}),
    libgstreamer-plugins-base1.0-0 (= ${binary:Version}),
    libgstreamer-gl1.0-0 (= ${binary:Version}),
    libgstreamer1.0-dev,
    pkg-config,
    libc-dev,
    libegl1-mesa-dev,
    libgl1-mesa-dev,
    libgles2-mesa-dev,
    libglib2.0-dev,
    liborc-0.4-dev,
Description: GStreamer development files for libraries from the "base" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains development files for GStreamer libraries from
 the "base" set, an essential exemplary set of elements.

Package: libgstreamer-gl1.0-0
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: gstreamer1.0-gl
Description: GStreamer GL libraries
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains the GL library.

Package: gstreamer1.0-alsa
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}, libgstreamer-plugins-base1.0-0 (>= ${binary:Version})
Suggests: alsa-utils
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
Provides: ${gstreamer:Provides}
Description: GStreamer plugin for ALSA
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains the GStreamer plugin for the ALSA library.  ALSA
 is the Advanced Linux Sound Architecture.

Package: gstreamer1.0-plugins-base
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}, libgstreamer-plugins-base1.0-0 (>= ${binary:Version})
Suggests: gvfs
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
XB-GStreamer-URI-Sources: ${gstreamer:URISources}
XB-GStreamer-URI-Sinks: ${gstreamer:URISinks}
XB-GStreamer-Encoders: ${gstreamer:Encoders}
XB-GStreamer-Decoders: ${gstreamer:Decoders}
Provides: ${gstreamer:Provides}
Description: GStreamer plugins from the "base" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains the GStreamer plugins from the "base" set, an
 essential exemplary set of elements.

Package: gstreamer1.0-x
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}, libgstreamer-plugins-base1.0-0 (>= ${binary:Version})
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
Provides: ${gstreamer:Provides}
Description: GStreamer plugins for X11 and Pango
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains the GStreamer plugins for X11 video output, both
 for standard Xlib support and for the Xv extension, as well as the
 plugin for Pango-based text rendering and overlay.

Package: gstreamer1.0-gl
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}, libgstreamer-plugins-base1.0-0 (>= ${binary:Version}), libgstreamer-gl1.0-0 (>= ${binary:Version})
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
Provides: ${gstreamer:Provides}
Description: GStreamer plugins for GL
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains the GStreamer plugins for GL.

Package: gir1.2-gst-plugins-base-1.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${gir:Depends}
Description: GObject introspection data for the GStreamer Plugins Base library
 This package contains introspection data for the GStreamer Plugins Base library.
 .
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.
